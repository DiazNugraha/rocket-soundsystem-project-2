<?php

namespace Database\Seeders;

use Carbon\Carbon;
use App\Models\User;
use App\Models\Product;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BookingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $product1 = Product::find('2');
        $product2 = Product::find('1');
        $product3 = Product::find('3');


        DB::table('bookings')->insert([
            "product_id" => $product1->id,
            'product_name' => $product1->name,
            "user_id" => 1,
            'user_email' => 'diaz@gmail.com',
            'start_date' => Carbon::createFromFormat('Y-m-d', '2021-06-10')->toDateTimeImmutable(),
            'end_date' => Carbon::createFromFormat('Y-m-d', '2021-06-11')->toDateTimeImmutable(),
            'total' => $product1->price,
            'book_address' => 'Kampus Politeknik Negeri Indonesia',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('bookings')->insert([
            "product_id" => $product2->id,
            'user_email' => 'diaznugraha@gmail.com',
            "user_id" => 3,
            'product_name' => $product2->name,
            'start_date' => Carbon::createFromFormat('Y-m-d', '2021-06-01')->toDateTimeImmutable(),
            'end_date' => Carbon::createFromFormat('Y-m-d', '2021-06-02')->toDateTimeImmutable(),
            'total' => $product2->price,
            'book_address' => 'Kampus Politeknik Negeri Balongan',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('bookings')->insert([
            "product_id" => $product3->id,
            "user_id" => 1,
            'user_email' => 'diaz@gmail.com',
            'product_name' => $product3->name,
            'start_date' => Carbon::createFromFormat('Y-m-d', '2021-05-28')->toDateTimeImmutable(),
            'end_date' => Carbon::createFromFormat('Y-m-d', '2021-05-29')->toDateTimeImmutable(),
            'total' => $product3->price,
            'book_address' => 'Kampus Politeknik Negeri Indramayu',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

    }
}
