<?php

namespace Database\Seeders;

use App\Models\Booking;
use Carbon\Carbon;
use Carbon\Doctrine\CarbonDoctrineType;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TransactionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $book1 = Booking::find('1');
        $book2 = Booking::find('2');
        $book3 = Booking::find('3');

        $pay1 = 2000000;
        $pay2 = 1500000;
        $pay3 = 1000000;

        DB::table('transactions')->insert([
            'booking_id' => 1,
            'transaction_status' => '1',
            'transaction_date' => Carbon::createFromFormat('Y-m-d', '2021-05-23')->toDateTimeImmutable(),
            'cashback' => $pay1 - $book1->total,
            'payment_amount' => $pay1,
        ]);

        DB::table('transactions')->insert([
            'booking_id' => 2,
            'transaction_status' => '2',
            'transaction_date' => Carbon::createFromFormat('Y-m-d', '2021-05-23')->toDateTimeImmutable(),
            'cashback' => $pay2 - $book2->total,
            'payment_amount' => $pay2,
        ]);

        DB::table('transactions')->insert([
            'booking_id' => 3,
            'transaction_status' => '1',
            'transaction_date' => Carbon::createFromFormat('Y-m-d', '2021-05-26')->toDateTimeImmutable(),
            'cashback' => $pay3 - $book3->total,
            'payment_amount' => $pay3,
        ]);

    }
}
