<?php

namespace App\Http\Livewire;

use App\Models\Product;
use Livewire\Component;

class Booking extends Component
{
    public $productId = '';

    public function render()
    {
        $product = Product::find($this->productId);
        dd($product);

        return view('livewire.booking');
    }
}
