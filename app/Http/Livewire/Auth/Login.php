<?php

namespace App\Http\Livewire\Auth;

use App\Models\User;
use Livewire\Component;
use Illuminate\Support\Facades\Auth;

class Login extends Component
{
    public $form = [
        'email' => '',
        'password' => ''
    ];

    public function render()
    {
        return view('livewire.auth.login');
    }

    public function login(){
        $this->validate([
            'form.email' => 'required|email',
            'form.password' => 'required'
        ]);
        $user = User::where([
            'email' => $this->form['email']
        ])->firstOrFail();

        if (Auth::attempt(['email' => $this->form['email'], 'password' => $this->form['password']])) {                                 
            if($user->role == 'admin'){
                return redirect()->route('admin');
            }
            return redirect()->route('/');
        }else{
            session()->flash('fail', 'Your credentials does not match');
            $this->form['email'] = '';
            $this->form['password'] = '';
        }        

    }
}
