<?php

namespace App\Http\Livewire\Auth;

use App\Models\User;
use Livewire\Component;

class Register extends Component
{

    public $form = [
        'name' => '',
        'email' => '',
        'phone' => '',
        'password' => '',
        'password_validation' => ''
    ];

    public function render()
    {
        return view('livewire.auth.register');
    }

    public function register(){
        $this->validate([
            'form.name' => 'required',
            'form.email' => 'required|email|unique:users,email',
            'form.phone' => 'required|numeric',
            'form.password' => 'required|confirmed'
        ]);

        User::create([
            'name' => $this->form['name'],
            'email' => $this->form['email'],
            'phone' => $this->form['phone'],
            'password' => bcrypt($this->form['password']),
            'role' => 'user'
        ]);

        session()->flash('success', 'Your account successfuly created');
        return redirect()->route('login');

    }

}
