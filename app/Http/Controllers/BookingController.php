<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use App\Models\Product;
use App\Models\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BookingController extends Controller
{
    public function index(){

    }

    public function booking($productId){
        $product = Product::find($productId);
        return view('booking', compact('product'));
    }

    public function createBook(Request $request){
        
        $request->validate([            
            'address' => 'required',
            'date' => 'required|date|date_format:Y-m-d|after:tomorrow',
            'period' => 'required',            
        ]);
        $product = Product::where('name', $request->productName)->get()->first();        
        
        if($product->quantity == 0){
            return redirect()->route('/')->with('info', 'Produk tidak tersedia');
        }        

        $date_formatted = (string) $request->date;
        $end_date = date('Y-m-d', strtotime($date_formatted . ' + ' . $request->period . ' days'));
        
        $booking = Booking::create([
            'user_name' => $request->name,
            'user_email' => $request->email,
            'product_name' => $request->productName,
            'start_date' => $request->date,
            'end_date' => $end_date,
            'total' => $request->total,
            'book_address' => $request->address
        ]);   
        Transaction::create([
            'booking_id' => $booking->id,
            'transaction_status' => 2,
            'transaction_date' => null,            
            'payment_amount' => $request->total
        ]);    

        return redirect()->route('history')->with('info', $booking->product_name . ' Booked Successfuly');
    }

    public function history(){
        $bookings = Booking::where('user_email', Auth::user()->email)->pluck('id')->toArray();        
        $transactions = Transaction::with(['booking'])->whereIn('booking_id', $bookings)->get();        
        return view('history', [
            'transactions' => $transactions,
            'state' => 'show'
        ]);
    }

    public function details($product_name){
        $product = Product::where('name', $product_name)->first();
        $bookings = Booking::where('user_email', Auth::user()->email)->pluck('id')->toArray();        
        $transactions = Transaction::with(['booking'])->whereIn('booking_id', $bookings)->get();              
        return view('history', [
            'transactions' => $transactions,
            'product' => $product,
            'state' => 'details'
        ]);
    }

}
