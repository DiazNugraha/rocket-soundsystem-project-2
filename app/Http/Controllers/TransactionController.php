<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Transaction;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class TransactionController extends Controller
{
    public function index(){

    }

    public function show($transaction_id){
        $transaction = Transaction::find($transaction_id);
        return view('transaction', compact('transaction'));
    }

    public function create(Request $request, $transaction_id){
        $request->validate([
            'image' => 'image|max:2048|required'
        ]);       
        
        $transaction_booking = Transaction::with('booking')->where('id', $transaction_id)->first();
        $imageName = md5($request->image.microtime())  . '.' . $request->image->extension();                        
        $product = Product::where('name', $transaction_booking->booking->product_name)->first();                

        Storage::putFileAs(
            'public/images',
            $request->image,
            $imageName
        );
               
        Transaction::where('id', $transaction_id)->update([
            'transaction_status' => 3,
            'transaction_date' => Carbon::now(),
            'image' => $imageName            
        ]);       

        $save = Product::where('name', $transaction_booking->booking->product_name)->update([
            'quantity' => $product->quantity - 1
        ]);        

        return redirect()->route('history')->with('info', 'Transaksi untuk peminjaman ' . $transaction_booking->booking->product_name . ' berhasil');
        
    }
}
