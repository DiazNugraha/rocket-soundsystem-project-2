<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index(){
        $products = Product::all();
        return view('admin.product', [
            'products' => $products,
            'state' => 'show'
        ]);
    }

    public function store(Request $request){
        $request->validate([
            'name' => 'required|unique:products,name',
            'price' => 'required|integer',
            'quantity' => 'required|integer',
            'description' => 'required',
            'power' => 'required',    
            'type' => 'required'
        ]);

        Product::create([
            'name' => $request->name,
            'price' => $request->price,
            'quantity' => $request->quantity,
            'description' => $request->description,
            'power' => $request->power,
            'type' => $request->type
        ]);

        return back()->with('info', 'Product created successfuly');
    }

    public function edit($id){
        $products = Product::all();
        $selected = Product::where('id', $id)->first();
        
        return view('admin.product', [
            'products' => $products,
            'selected' => $selected,
            'state' => 'edit'
        ]);
    }

    public function update(Request $request, $id){
        $request->validate([
            'name' => 'required',
            'price' => 'required|integer',
            'quantity' => 'required|integer',
            'description' => 'required',
            'power' => 'required'
        ]);

        $product = Product::where('id', $id)->first();
        $product->update([
            'name' => $request->name,
            'price' => $request->price,
            'quantity' => $request->quantity,
            'description' => $request->description,
            'power' => $request->power
        ]);

        return redirect()->route('product.index')->with('info', 'Product was updated');

    }

    public function destroy($id){
        $product = Product::where('id', $id)->first();
        $product->delete();
        return back()->with('info', 'Product deleted successfuly');
    }

}
