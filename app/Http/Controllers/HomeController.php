<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Database\Seeders\ProductSeeder;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $products = Product::all();
        return view('home', compact('products'));
    }

    public function sortSoundsystem(){
        $products = Product::where('type', 'soundsystem')->get();
        return view('home', compact('products'));
    }

    public function sortBand(){
        $products = Product::where('type', 'band')->get();
        return view('home', compact('products'));
    }

    public function sortGenset(){
        $products = Product::where('type', 'genset')->get();
        return view('home', compact('products'));
    }

}
