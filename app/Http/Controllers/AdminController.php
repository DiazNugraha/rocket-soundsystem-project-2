<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Product;
use App\Models\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{
    public function index(){
        $products = Product::count();        
        $income = Transaction::where('transaction_status', '1')->sum('payment_amount');
        $waiting = Transaction::where('transaction_status', '3')->count();
        $proceed = Transaction::where('transaction_status', '1')->count();        
        $user = Auth::user();        
        return view('admin.dashboard', [
            'user' => $user,
            'income' => $income,
            'products' => $products,
            'waiting' => $waiting,
            'proceed' => $proceed
        ]);
    }

    public function logout(){
        Auth::logout();
        return redirect()->to('/');
    }

    public function userIndex(){
        $users = User::all()->where('role', 'user');
        return view('admin.user', compact('users'));
    }

    public function addUser(Request $request){
        $request->validate([
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'phone' => 'required|numeric',
            'password' => 'required|confirmed'
        ]);

        $password_hash = Hash::make($request->password);
        
        User::create([
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'password' => $password_hash,
            'role' => 'user'
        ]);        

        return back()->with('info', 'User created successfuly');     
    }

    public function userDestroy($id){
        $user = User::where('id', $id)->first();
        $user->delete();
        return back()->with('info', 'User deleted successfuly');
    }

    public function getAllcustomer(){
        $transactions = Transaction::with('booking')->get();                        
        $user = Auth::user();        
        return view('admin.customer', [
            'user' => $user,
            'state' => 'all',
            'transactions' => $transactions
        ]);
    }

    public function getPendingCustomer(){
        $transactions = Transaction::with('booking')->where('transaction_status', '2')->get();                        
        $user = Auth::user();        
        return view('admin.customer', [
            'user' => $user,
            'state' => 'pending',
            'transactions' => $transactions
        ]);
    }

    public function getProceedCustomer(){
        $transactions = Transaction::with('booking')->where('transaction_status', '1')->get();                        
        $user = Auth::user();        
        return view('admin.customer', [
            'user' => $user,
            'state' => 'proceed',
            'transactions' => $transactions
        ]);
    }    

    public function getWaitingCustomer(){
        $transactions = Transaction::with('booking')->where('transaction_status', '3')->get();
        $user = Auth::user();
        return view('admin.customer', [
            'user' => $user,
            'state' => 'waiting',
            'transactions' => $transactions
        ]);
    }

    public function getRejectedCustomer(){
        $transactions = Transaction::with('booking')->where('transaction_status', '4')->get();
        $user = Auth::user();
        return view('admin.customer', [
            'user' => $user,
            'state' => 'waiting',
            'transactions' => $transactions
        ]);
    }

    public function acceptCustomer($transaction_id){        
        $transaction = Transaction::find($transaction_id);
        $transaction->update([
            'transaction_status' => 1
        ]);
        return redirect()->route('customer.all')->with('info', 'Transaksi berhasil di perbarui');
    }

    public function rejectCustomer($transaction_id){
        $transaction = Transaction::find($transaction_id);
            $transaction->update([
                'transaction_status' => 4
            ]);
            return redirect()->route('customer.all')->with('info', 'Transaksi berhasil di perbarui');
    }

    public function deleteCustomer($transaction_id){
        $transaction = Transaction::with('booking')->find($transaction_id);
        $product = Product::where('name', $transaction->booking->product_name)->firstOrFail();


        $product->update([
            'quantity' => $product->quantity + 1
        ]);

        $transaction->booking->delete();

        $transaction->delete();
        return redirect()->route('customer.all')->with('info', 'Transaksi berhasil di perbarui');
    }


}
