<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use HasFactory;
    protected $fillable = [
        'booking_id',
        'transaction_status',
        'transaction_date',
        'cashback',
        'payment_amount'
    ];

    public $timestamps = false;

    public function booking(){
        return $this->hasOne(Booking::class, 'id', 'booking_id');
    }
}
