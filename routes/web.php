<?php

use App\Http\Livewire\Auth\Login;
use App\Http\Livewire\Auth\Register;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\BookingController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\HomeController;
use App\Http\Livewire\Auth\Logout;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\TransactionController;
use App\Models\Transaction;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('dashboard');
});

Route::middleware('guest')->group(function(){
    Route::get('/login', Login::class)->name('login');
    Route::get('/register', Register::class)->name('register');
    Route::get('/', [DashboardController::class, 'index'])->name('/');
    Route::get('/landingPage', [DashboardController::class, 'index'])->name('/landingPage');
    
});
// Auth::routes();

Route::middleware('auth')->group(function(){    
    
    Route::get('home', [HomeController::class, 'index'])->name('/');    
    Route::get('logout', [Logout::class, 'logout'])->name('logout');
    Route::prefix('product')->group(function(){
        Route::get('soundsystem', [HomeController::class, 'sortSoundsystem'])->name('product.soundsystem');
        Route::get('band', [HomeController::class, 'sortBand'])->name('product.band');
        Route::get('genset', [HomeController::class, 'sortGenset'])->name('product.genset');
    });

    Route::get('booking/{productId}', [BookingController::class, 'booking'])->name('booking');    
    Route::post('createBook', [BookingController::class, 'createBook'])->name('createBook');    
    Route::get('history', [BookingController::class, 'history'])->name('history');
    Route::get('details/{productName}', [BookingController::class, 'details'])->name('details');

    Route::prefix('transaction')->group(function(){
        Route::get('show/{transactionId}', [TransactionController::class, 'show'])->name('transaction.show');        
        Route::post('create/{transactionId}', [TransactionController::class, 'create'])->name('transaction.create');
    });
    


    Route::middleware(['admin'])->group(function(){
        Route::get('admin', [AdminController::class, 'index'])->name('admin');
        Route::get('logout', [AdminController::class, 'logout'])->name('logout');

        Route::prefix('product')->group(function(){
            Route::get('index', [ProductController::class, 'index'])->name('product.index');
            Route::get('edit/{id}', [ProductController::class, 'edit'])->name('product.edit');
            Route::post('create', [ProductController::class, 'store'])->name('product.create');
            Route::put('update/{id}', [ProductController::class, 'update'])->name('product.update');
            Route::get('delete/{id}', [ProductController::class, 'destroy'])->name('product.delete');
        });

        Route::prefix('user')->group(function(){
            Route::get('user', [AdminController::class, 'userIndex'])->name('user.index');
            Route::post('create', [AdminController::class, 'addUser'])->name('user.create');           
            Route::get('delete/{id}', [AdminController::class, 'userDestroy'])->name('user.delete');
        });        

        Route::prefix('customer')->group(function(){
            Route::get('allCustomer', [AdminController::class, 'getAllCustomer'])->name('customer.all');
            Route::get('pendingCustomer', [AdminController::class, 'getPendingCustomer'])->name('customer.pending');
            Route::get('proceedCustomer', [AdminController::class, 'getProceedCustomer'])->name('customer.proceed');
            Route::get('waiting', [AdminController::class, 'getWaitingCustomer'])->name('customer.waiting');
            Route::get('reject', [AdminController::class, 'getRejectedCustomer'])->name('customer.rejected');
            Route::get('accept/{transactionId}', [AdminController::class, 'acceptCustomer'])->name('customer.accept');
            Route::get('reject/{transactionId}', [AdminController::class, 'rejectCustomer'])->name('customer.reject');
            Route::get('delete/{transactionId}', [AdminController::class, 'deleteCustomer'])->name('customer.delete');
        });
    });  
       

});
