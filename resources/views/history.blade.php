@extends('layouts.app2')
@section('content')



<div class="container">


<!-- Content Row -->
<!-- alert -->

<div class="row mt-4">
<!-- Area Chart -->
    <div class="col-xl-9 col-lg-9 ml-5">
    <h1 class="ml-3">Booking</h1>
        <div class="card shadow mb-4">                                
            <div class="card-body text-center">                
                @if(session('info'))
                    <div class="alert alert-success">{{ session('info') }}</div>
                @endif
                <div class="row mt-4 ml-md-3 mr-md-3">
                <div class="table-resposive justify-content-center">
                        <table class="table table-bordered text-center" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Product</th>
                                    <th>Tanggal Pinjam</th>
                                    <th>Tanggal Pengembalian</th>
                                    <th>Harga Total</th>
                                    <th>Status Transaksi</th>     
                                    <th>Aksi</th>                           
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($transactions as $index=>$transaction)
                                <tr>
                                    <td>{{ $index + 1 }}</td>
                                    <td>{{ $transaction->booking->product_name }}</td>
                                    <td>{{ $transaction->booking->start_date }}</td>
                                    <td>{{ $transaction->booking->end_date }}</td>
                                    <th>Rp. {{ number_format($transaction->booking->total, 2) }}</th>
                                    @if($transaction->transaction_status == 1)
                                        <th>{{ 'Diproses' }}</th>
                                    @endif
                                    @if($transaction->transaction_status == 2)
                                        <th>{{ 'Pending' }}</th>
                                    @endif   
                                    @if($transaction->transaction_status == 3)
                                        <th>{{ 'Menunggu' }}</th>
                                    @endif   
                                    @if($transaction->transaction_status == 4)
                                        <th>{{ 'Ditolak' }}</th>
                                    @endif   
                                    <th>
                                        <div class="col-sm justify-content-center">                                            
                                            @if($transaction->transaction_status == 1)
                                                <a href="{{ route('details', $transaction->booking->product_name) }}" class="btn btn-sm btn-primary mb-2 btn-block">Detail</a>
                                            @endif
                                            @if($transaction->transaction_status == 2)
                                                <a href="{{ route('details', $transaction->booking->product_name) }}" class="btn btn-sm btn-primary mb-2 btn-block">Detail</a>
                                                <a href="{{ route('transaction.show', $transaction->id) }}" class="btn btn-sm btn-success btn-block">Transaksi</a>                                            
                                            @endif  
                                            @if($transaction->transaction_status == 3)
                                                <a href="{{ route('details', $transaction->booking->product_name) }}" class="btn btn-sm btn-primary mb-2 btn-block">Detail</a>
                                            @endif  
                                            @if($transaction->transaction_status == 4)
                                                <a href="#" class="btn btn-sm btn-danger mb-2 btn-block">Hapus</a>
                                            @endif                                          
                                        </div>
                                    </th>                                                         
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>                    
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-2">
    <a href="{{ route('/') }}" class="btn btn-outline-dark btn-block mb-3 shadow-lg"><i class="fa fa-arrow-circle-left"></i> Back</a>
        <div class="card shadow mb-4">
            <!-- Card Header - Dropdown -->
            <div
                class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 font-weight-bold text-primary">Detail produk</h6>
                <div class="dropdown no-arrow">
                    
                </div>
            </div>
            <!-- Card Body -->
            <div class="card-body">                        
                    <div class="text-center">
                        @if($state == 'show')
                            <img src="{{ asset('images/list_history.svg') }}" class="" style="height:150px; width:100px;" alt="">
                        @endif
                        @if($state == 'details')
                            <p>Nama : {{$product->name}}</p>
                            <p>Harga sewa per hari : {{$product->price}}</p>
                            <p>Deskripsi : {{$product->description}}</p>
                            <p>Daya : {{ $product->power }}</p>     
                        @endif                                                               
                    </div>            
                    <div class="mt-4 text-center small">
                        <span class="mr-2">
                            <i class="fas fa-circle text-primary"></i>
                        </span>
                        <span class="mr-2">
                            <i class="fas fa-circle text-success"></i>
                        </span>
                        <span class="mr-2">
                            <i class="fas fa-circle text-info"></i>
                        </span>
                    </div>
                    
            </div>
        </div>
    </div>
</div>
@endsection
