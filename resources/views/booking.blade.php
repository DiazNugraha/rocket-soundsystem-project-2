@extends('layouts.app2')
@section('content')



<div class="container">



<!-- Content Row -->
<!-- alert -->
<div class="row mt-4">
<!-- Area Chart -->
<div class="col-md-9 col-md-9">
<h1 class="">Booking</h1>
    <div class="card shadow mb-4 justify-content-center">        
        <div class="card-body text-center">
            <div class="row mt-4 ml-md-2 mr-md-2">
            <div class="card-body">
            
            <form action="{{ route('createBook') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="form-group row">
                  <label for="productName" class="col-sm-2 col-form-label">Nama Produk</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="productName" name="productName" placeholder="masukkan nama anda..." value="{{ $product->name }}" readonly>                   
                    </div>
                </div>
                <div class="form-group row">
                  <label for="name" class="col-sm-2 col-form-label">Nama Pemesan</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="name" name="name" placeholder="masukkan nama anda..." value="{{ Auth::user()->name }}" readonly>                   
                  </div>
                </div>
                <div class="form-group row">
                  <label for="email" class="col-sm-2 col-form-label">Email</label>
                  <div class="col-sm-10">
                    <input type="email" class="form-control" id="email" name="email" placeholder="masukkan email anda..." value="{{ Auth::user()->email }}" readonly>
                  </div>                  
                </div>
                <div class="form-group row">
                    <label for="address" class="col-sm-2 col-form-label">Alamat Lengkap (wilayah Indramayu)</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="address" name="address" placeholder="masukkan alamat anda...">
                      @error('address')
                        <small class="text-danger">{{ $message }}</small>
                      @enderror
                    </div>                    
                </div>
                <div class="form-group row">
                    <label for="date" class="col-sm-2 col-form-label">Tanggal Sewa</label>
                    <div class="col-sm-10">
                      <input type="date" class="form-control" id="date" name="date" placeholder="masukkan nama kota...">
                      @error('date')
                        <small class="text-danger">{{ $message }}</small>
                      @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <label for="period" class="col-sm-2 col-form-label">Lama Sewa (Hari)</label>
                    <div class="col-sm-10">
                      <input type="number" class="form-control" id="period" min="1" value="1" name="period" onchange="calculate()" placeholder="masukkan lama sewa...">
                      @error('period')
                        <small class="text-danger">{{ $message }}</small>
                      @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <label for="total" class="col-sm-2 col-form-label">Total Harga</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="total" value="{{ $product->price }}" name="total" placeholder="0" readonly>
                    </div>
                </div>                
                <div class="form-group row">
                  <div class="col-sm-2 col-form-label"></div>
                  <div class="col-sm-10">                    
                    <button type="submit" class="btn btn-success btn-block"><i class="fas fa-save fa-lg"></i> Kirim</button>
                  </div>
                </div>                
              </form>
        </div>                         
            </div>            
        </div>        
    </div>
    </div>
    <div class="col-md-3">
    <a href="{{ route('/') }}" class="btn btn-outline-dark btn-block mb-3 shadow-lg"><i class="fa fa-arrow-circle-left"></i> Back</a>
        <div class="card shadow mb-4">
            <!-- Card Header - Dropdown -->
            <div
                class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 font-weight-bold text-primary">Booking</h6>
                <div class="dropdown no-arrow">
                    
                </div>
            </div>
            <!-- Card Body -->
            <div class="card-body">                        
                    <div class="text-center">                       
                            <img src="{{ asset('images/booking.svg') }}" class="" style="height:250px; width:200px;" alt="">                        
                    </div>            
                    <div class="mt-4 text-center small">
                        <span class="mr-2">
                            <i class="fas fa-circle text-primary"></i>
                        </span>
                        <span class="mr-2">
                            <i class="fas fa-circle text-success"></i>
                        </span>
                        <span class="mr-2">
                            <i class="fas fa-circle text-info"></i>
                        </span>
                    </div>
            
            </div>
</div>
@endsection

@push('custom-script')
    <script type="text/javascript">          
        function calculate(){
            const price = <?php echo  $product->price ?>;
            const period = document.getElementById('period').value;
            var total = period * price;
            document.getElementById('total').value = total;
        }
    </script>

@endpush

