<div class="container">
    <div class="row">
    <a class="navbar-brand" href="{{ url('/') }}">
                    <i class="fas fa-feather-alt"></i> Rocket
                </a>
    </div>
    <div class="row">
    <div class="col">
        <div class="row">            
            <div class="col-4">
                <div class="row">            
                <div class="col-md-4">
                        <img src="{{ asset('images/login.svg') }}" class="rounded float-left" width="500" height="500" alt="...">
                </div>
                </div>
            </div>

            <div class="col-8">
                <div class="row">
                    <div class="col-md-4"></div>
                    <div class="col-md-7">
                        <div class="card mt-5">            
                            <div class="card-body">
                                <h1 class="text-center">Login</h1>
                                @if(session('success'))
                                    <div class="alert alert-success">{{ session('success') }}</div>
                                @endif  
                                @if(session('fail'))
                                    <div class="alert alert-danger">{{ session('fail') }}</div>
                                @endif
                                <form wire:submit.prevent="login" action="#">
                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        <input wire:model="form.email" type="text" id="email" placeholder="user@example.com" class="form-control">
                                        @error('form.email')
                                            <small class="text-danger">{{ $message }}</small>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="pass">Password</label>
                                        <input wire:model="form.password" type="password" id="pass" class="form-control" placeholder="*****">
                                        @error('form.password')
                                            <small class="text-danger">{{ $message }}</small>                        
                                        @enderror
                                    </div>
                                    <div class="form-group mt-2">
                                        <button class="btn btn-primary btn-block">Login</button>
                                    </div>                    
                                </form>
                                <div class="justify-content-center">
                                    <p>Belum punya akun?
                                        <a href="{{ route('register') }}">Daftar disini</a>
                                    </p>
                                    <p>Kembali ke Halaman awal :
                                        <a href="{{ route('/') }}">Halaman Awal</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4"></div>
                </div>
                </div>            
        </div>
    </div>
    </div>
</div>

  