<div class="container"> 
    <div class="row">
    <a class="navbar-brand" href="{{ url('/') }}">
                <i class="fas fa-feather-alt"></i> Rocket
                </a>
    </div>
    <div class="row">
    <div class="col">
        <div class="row">            
            <div class="col-4">
                <div class="row">            
                <div class="col-md-4">
                        <img src="{{ asset('images/register.svg') }}" class="rounded float-left" width="500" height="500" alt="...">
                </div>
                </div>
            </div>

            <div class="col-8">
                <div class="row">
                    <div class="col-md-4"></div>
                    <div class="col-md-7">
                        <div class="card">            
                            <div class="card-body">
                                <h1 class="text-center">Registrasi</h1>
                                @if(session('fail'))
                                    <div class="alert alert-danger">{{ session('fail') }}</div>
                                @endif
                                <form wire:submit.prevent="register" action="#">
                                    <div class="form-group">
                                        <label for="name">Name</label>
                                        <input type="text" id="name" wire:model="form.name" class="form-control" placeholder="Your name ...">
                                        @error('form.name')
                                            <small class="text-danger">{{ $message }}</small>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        <input wire:model="form.email" type="text" id="email" placeholder="user@example.com" class="form-control">
                                        @error('form.email')
                                            <small class="text-danger">{{ $message }}</small>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="phone">Phone Number</label>
                                        <input type="text" id="phone" placeholder="08**********" class="form-control" wire:model="form.phone">
                                        @error('form.phone')
                                            <small class="text-danger">{{ $message }}</small>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="pass">Password</label>
                                        <input wire:model="form.password" type="password" id="pass" class="form-control" placeholder="*****">
                                        @error('form.password')
                                            <small class="text-danger">{{ $message }}</small>                        
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="confirmPass">Password Confirmation</label>
                                        <input type="password" id="confirmPass" class="form-control" wire:model="form.password_confirmation" placeholder="*****">
                                        @error('form.password_confirmation')
                                            <small class="text-danger">{{ $message }}</small>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <button class="btn btn-primary btn-block">Register</button>
                                    </div>                    
                                </form>
                                <p>Sudah punya akun?
                                    <a href="{{ route('login') }}">Login disini</a>
                                </p>
                                <p>Kembali ke Halaman awal :
                                    <a href="{{ route('/landingPage') }}">Halaman Awal</a>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4"></div>
                </div>
                </div>            
        </div>
    </div>
    </div>
</div>

  
