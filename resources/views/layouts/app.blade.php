@extends('layouts.base')
@section('baseStyles')
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/dashboard.css') }}" rel="stylesheet">    
    <link rel="preconnect" href="https://fonts.gstatic.com">    
    <link href="https://fonts.googleapis.com/css2?family=Viga&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.0-2/css/fontawesome.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.0-2/css/all.min.css" />
    @livewireStyles
    <title>Rocket SoundSystem</title>
@endsection
@section('baseScripts')
    <script src="{{ asset('js/app.js') }}"></script>
@endsection

@section('body')
        @if(!Route::is('login') and !Route::is('register'))
            <x-navbar></x-navbar>
        @endif        
        
        <main class="py-4">
            @yield('content')
            <div class="container-fluid">
            {{isset($slot) ? $slot : null}}
            </div>            
        </main>
    </div>
    @livewireScripts
        <script src="https://cdn.jsdelivr.net/gh/livewire/turbolinks@v0.1.x/dist/livewire-turbolinks.js" data-turbolinks-eval="false" data-turbo-eval="false"></script>
        @stack('custom-script')

@endsection