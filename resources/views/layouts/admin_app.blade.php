<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">    
    <link href="{{ asset('css/sb-admin-2.min.css') }}" rel="stylesheet">    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.0-2/css/fontawesome.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.0-2/css/all.min.css" />
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Viga&display=swap" rel="stylesheet">
    @livewireStyles
    <title>Admin</title>   

</head>
<body id="page-top">           
<div id="app">
    <x-navbar-admin></x-navbar-admin>
        <main class="py-4">
            @yield('content')
            <div class="container-fluid">
            {{isset($slot) ? $slot : null}}
            </div>            
        </main>         
    
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
