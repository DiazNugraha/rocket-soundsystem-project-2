@extends('layouts.base')
@section('baseStyles')    
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/dashboard.css') }}" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Viga&display=swap" rel="stylesheet">
@endsection

@section('body')    
    <x-navbar></x-navbar>
    @yield('content')    
@endsection

@section('baseScripts')
    <script src="{{ asset('js/app.js') }}"></script>
    @stack('scripts')
@endsection