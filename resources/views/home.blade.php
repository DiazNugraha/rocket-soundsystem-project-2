@extends('layouts.app')

@section('content')
    
          <div class="jumbotron jumbotron-fluid">
            <div class="container">
              <!-- <a href="" class="btn centerText">See More</a>
              <h1 class="display-6">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quaerat, officiis.</h1> -->
              <h1 class="centerText">Rocket soundsystem</h1>
            </div>
          </div>
          <!-- container -->
          <div class="container">
            <div class="row justify-content-center">
              <div class="col-10 info-panel">
                <div class="row">
                  <div class="col-lg">
                    <a href="{{ route('product.soundsystem') }}">
                        <img src="{{ asset('images/icon-soundsystem.png') }}" alt="Employee" class="float-left">
                        <h4>Sound System</h4>
                        <p>Lorem ipsum dolor sit amet.</p>
                    </a>                    
                  </div>
                  <div class="col-lg">
                    <a href="{{ route('product.band') }}">
                        <img src="{{ asset('images/icon-band.png') }}" alt="Employee" class="float-left">
                        <h4>Alat Band</h4>
                        <p>Lorem ipsum dolor sit amet.</p>
                    </a>
                  </div>
                  <div class="col-lg">
                    <a href="{{ route('product.genset') }}">
                        <img src="{{ asset('images/icon-genset.png') }}" alt="Employee" class="float-left">
                        <h4>Genset</h4>
                        <p>Lorem ipsum dolor sit amet.</p>
                    </a>
                  </div>
                </div>
              </div>
            </div>

            <!-- Workspace  -->
            <div class="card-body">
            @if(session('info'))
              <div class="alert alert-danger">{{ session('info') }}</div>
            @endif
            <div class="row justify-content-center">
            @forelse($products as $index=>$product)
              <div class="col-md-3 mb-3">                
                  <div class="card" style="cursor: pointer;">
                          @if($product->type == 'soundsystem')
                          <img src="{{ asset('images/icon-soundsystem.png') }}" alt="Product Image" style="width: 100%;height:170px;object-fit:contain;">
                          @endif
                          @if($product->type == 'band')
                          <img src="{{ asset('images/icon-band.png') }}" alt="Product Image" style="width: 100%;height:170px;object-fit:contain;">
                          @endif
                          @if($product->type == 'genset')
                          <img src="{{ asset('images/icon-genset.png') }}" alt="Product Image" style="width: 100%;height:170px;object-fit:contain;">
                          @endif
                          <a class="btn btn-primary btn-sm" href="{{ route('booking', $product->id) }}" style="position: absolute;top:0;right:0;padding:10px 15px;"><i class="fas fa-cart-plus fa-lg"></i></a>                                                                
                          <h6 class="text-center font-weight-bold mt-2">{{ $product->name }}</h6>                                    
                          <h6 class="text-center font-weight-bold" style="color: grey;">Rp. {{ number_format( $product->price ,2,",",".") }}</h6>                                
                          <h6 class="text-center font-weight-bold" style="color: grey;">Tersedia : {{ $product->quantity }}</h6>
                          <a href="{{ route('booking', $product->id) }}" class="btn btn-primary btn-block">Sewa</a>
                  </div>               
              </div>    
            @empty
              <div class="col-sm-12 mt-5">
                <h2 class="text-center font-weight-bold">No Products Found</h2>
              </div>
            @endforelse          
            </div>
              
           </div>
          </div>
          <br>
          <br>

     <!-- Footer -->
     <footer class="page-footer font-small blue pt-4" style="background-color: #292b2c;">

<!-- Footer Links -->
<div class="container-fluid text-center text-md-left">

  <!-- Grid row -->
  <div class="row">

    <!-- Grid column -->
    <div class="col-md-6 mt-md-0 mt-3">

      <!-- Content -->
      <h5 class="text-uppercase text-light">Rocket Soundsystem</h5>      
      <p class="text-light">adalah salah satu penyedia jasa penyewaan sound system dan alat-alatnya untuk sekitar indramayu</p>

    </div>
    <!-- Grid column -->

    <hr class="clearfix w-100 d-md-none pb-3">

    <!-- Grid column -->
    <div class="col-md-3 mb-md-0 mb-3">

      <!-- Links -->
      <h5 class="text-uppercase text-light">Link to :</h5>

      <ul class="list-unstyled">
        <li>
          <a href="#">Home</a>
        </li>
        <li>
          <a href="#">About</a>
        </li>
        <li>
        <br>
          <p class="text-light">Contact Us : </p>
          <a href="https://wa.me/081915384400" target="_blank">https://wa.me/087732802699</a>
        </li>
      </ul>

    </div>
    <!-- Grid column -->   
    <!-- Grid column -->
  </div>
  <!-- Grid row -->
</div>
<!-- Footer Links -->
<!-- Copyright -->
<div class="footer-copyright text-center py-3 text-light">© 2021 Copyright:
  <a href=""> Rocket Soundsystem</a>
</div>
<!-- Copyright -->

</footer>
<!-- Footer -->

@endsection