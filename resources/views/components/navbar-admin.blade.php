<div id="wrapper">
        <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
                <div class="sidebar-brand-icon rotate-n-15">
                    <!-- <i class="fas fa-laugh-wink"></i> -->
                    <i class="fas fa-feather-alt"></i>
                </div>
                <div class="sidebar-brand-text mx-3">Rocket</div>
            </a>
            <br>
            <br>
            <br>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <!-- Nav Item - Dashboard -->
            <li class="nav-item">
                <a class="nav-link" href="{{ route('admin') }}">
                    <i class="fas fa-fw fa-tachometer-alt"></i>
                    <span>Dashboard</span></a>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider">
            <!-- Nav Item - Dashboard -->
            <li class="nav-item">
                <a class="nav-link" href="{{ route('product.index') }}">
                    <i class="fas fa-fw fa-briefcase"></i>
                    <span>Produk</span></a>
            </li>
            <!-- Divider -->
            <hr class="sidebar-divider">
            <!-- Nav Item - Dashboard -->
            <li class="nav-item">
                <a class="nav-link" href="{{ route('user.index') }}">
                    <i class="fas fa-fw fa-address-book"></i>
                    <span>Pengguna</span></a>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider">
            <!-- Nav Item - Dashboard -->
             <!-- Heading -->
             <div class="sidebar-heading">
                Pesanan
            </div>

            <!-- Nav Item - Pages Collapse Menu -->
            <li class="nav-item">
                <a class="nav-link collapsed" href="" data-toggle="collapse" data-target="#collapseTwo"
                    aria-expanded="true" aria-controls="collapseTwo">
                    <i class="fas fa-tshirt"></i>
                    <span>Customer</span>
                </a>
                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <h6 class="collapse-header">Status Pelanggan</h6>                        
                        <a class="collapse-item" href="{{ route('customer.all') }}">All Customer</a>
                        <a class="collapse-item" href="{{ route('customer.waiting') }}">Menunggu</a>
                        <a class="collapse-item" href="{{ route('customer.pending') }}">Pending</a>
                        <a class="collapse-item" href="{{ route('customer.proceed') }}">Diproses</a>
                        <a class="collapse-item" href="{{ route('customer.rejected') }}">Ditolak</a>
                        <!-- <a class="collapse-item" href="#">Terlewati</a> -->
                    </div>
                </div>
            </li>
           
        </ul>
        <!-- End of Sidebar -->
        <div id="content-wrapper" class="d-flex flex-column">
            <div id="content">              
                    <nav class="navbar navbar-expand-lg navbar-light">
                        <div class="container">

                        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                        <div class="navbar-nav ml-auto">                        
                            <div class="dropdown">
                            <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-user fa-fw mr-3"></i>     
                                {{ Auth::user()->name }}                         
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">                                                
                                <a class="dropdown-item" href="{{ route('logout') }}">Logout</a>
                            </div>
                            </div>
                        </div>
                        </div>
                  </nav>    