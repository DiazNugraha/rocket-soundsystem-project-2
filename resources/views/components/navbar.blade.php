<nav class="navbar navbar-expand-md navbar-dark bg-dark shadow-lg">
            <div class="container-fluid">

                <a class="navbar-brand" href="{{ url('/') }}">
                    <i class="fas fa-feather-alt"></i> Rocket
                </a>               
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">                        
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav mr-5">
                        <!-- Authentication Links -->
                        @guest
                        
                        <li class="nav-item">
                            <a class="nav-link btn btn-sm btn-block btn-outline-primary shadow p-3 rounded-pill" href="{{ route('login') }}">Join Us</a>
                        </li>
                        
                            
                        @else
                            @if(Auth::user()->role == 'user')
                                <li class="nav-item">
                                    <a class="nav-link btn btn-sm btn-outline-primary shadow p-3 rounded-pill" href="{{ route('/') }}">Home</a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('history') }}" class="nav-link btn btn-sm btn-outline-primary shadow p-3 rounded-pill">Booking</a>
                                </li>
                            @endif
                            
                            @if(Auth::user()->role == 'admin')
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('admin') }}">Admin Dashboard</a>
                                </li>
                            @endif
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }}
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                   <livewire:auth.logout />                                   
                                </div>                                
                            </li>

                        @endguest
                       
                    </ul>
                </div>
            </div>
        </nav>
