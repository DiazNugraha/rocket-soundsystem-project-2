@extends('layouts.app2')
@section('content')



<div class="container">



<!-- Content Row -->
<!-- alert -->
<div class="row mt-4">
<!-- Area Chart -->
<div class="col-md-9 col-md-9">
<h1 class="">Transaction</h1>
    <div class="card shadow mb-4 justify-content-center">        
        <div class="card-body text-center">
            <div class="row mt-4 ml-md-2 mr-md-2">
            <div class="card-body">
            
            <form action="{{ route('transaction.create', $transaction->id) }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="form-group row">   
                    <label for="customFile">Masukkan gambar bukti transaksi</label>                 
                    <input type="file" name="image" class="form-control" id="customFile" />                      
                    @error('image')
                        <small class="text-danger">{{ $message }}</small>
                    @enderror
                </div>
                <div class="form-group row">
                  <div class="col-sm-2 col-form-label"></div>
                  <div class="col-sm-10">                    
                    <button type="submit" class="btn btn-success btn-block"><i class="fas fa-save fa-lg"></i> Kirim</button>
                  </div>
                </div>                                             
              </form>
        </div>                         
            </div>            
        </div>        
    </div>
    </div>
    <div class="col-md-3">
    <a href="{{ route('history') }}" class="btn btn-outline-dark btn-block mb-3 shadow-lg"><i class="fa fa-arrow-circle-left"></i> Back</a>
        <div class="card shadow mb-4">
            <!-- Card Header - Dropdown -->
            <div
                class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 font-weight-bold text-primary">Transaction</h6>
                <div class="dropdown no-arrow">
                    
                </div>
            </div>
            <!-- Card Body -->
            <div class="card-body">                        
                    <div class="text-center">                       
                            <img src="{{ asset('images/booking.svg') }}" class="" style="height:250px; width:200px;" alt="">                        
                    </div>            
                    <div class="mt-4 text-center small">
                        <span class="mr-2">
                            <i class="fas fa-circle text-primary"></i>
                        </span>
                        <span class="mr-2">
                            <i class="fas fa-circle text-success"></i>
                        </span>
                        <span class="mr-2">
                            <i class="fas fa-circle text-info"></i>
                        </span>
                    </div>
            
            </div>
</div>
@endsection


