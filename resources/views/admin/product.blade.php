@extends('layouts.admin_app')
@section('content')

<div>
    <div class="row">
        <div class="col-md-8">
           
            <div class="card shadow mb-4 ml-3">
                <div class="card-body">
                    <h2 class="font-weight-bold mb-3">Daftar Produk</h2>
                    @if(session('info'))
                        <div class="alert alert-success">{{ session('info') }}</div>
                    @endif
                    <table class="table table-bordered table-hovered table-striped text-center">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama Paket</th>
                                <th>Harga</th>
                                <th>Jumlah</th>
                                <th>Deskripsi</th>
                                <th>Daya</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($products as $index=>$product)
                            <tr>
                                <td>{{ $index + 1 }}</td>
                                <td>{{ $product->name }}</td>
                                <td>{{ $product->price }}</td>
                                <td>{{ $product->quantity }}</td>
                                <td>{{ $product->description }}</td>
                                <td>{{ $product->power }}</td>
                                <td>
                                    <a href="{{ route('product.edit', $product->id) }}" class="btn btn-success">Edit</a>
                                    <a href="{{ route('product.delete', $product->id) }}" class="btn btn-danger">Hapus</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card shadow mb-4">
                <div class="card-body">
                @if($state == 'show')
                    <h2 class="font-weight-bold mb-3">Tambah Produk</h2>
                    <form action="{{ route('product.create') }}" method="post" enctype="multipart/form-data">
                    @csrf                 
                    @method('post')   
                        <div class="form-group">
                            <label for="name">Nama Paket</label>
                            <input type="text" id="name" name="name" class="form-control">
                            @error('name')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="price">Harga Paket</label>
                            <input type="text" id="price" class="form-control" name="price">
                            @error('price')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror                           
                        </div>
                        <div class="form-group"> 
                            <label for="quantity">Jumlah Paket</label>
                            <input type="number" id="quantity" class="form-control" name="quantity">
                            @error('quantity')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror         
                        </div>
                        <div class="form-group">
                            <label for="description">Deskripsi</label>
                            <textarea id="description" class="form-control" name="description"></textarea>
                            @error('description')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="power">Daya</label>
                            <input type="number" id="power" class="form-control" name="power">
                            @error('power')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="type">Jenis</label>
                            <input type="text" id="type" class="form-control" name="type"> 
                            @error('type')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-block">Submit</button>        
                        </div>
                    </form>   
                    @endif
                    @if($state == 'edit')                 
                    <h2 class="font-weight-bold mb-3">Edit Produk &emsp;&emsp;&emsp;<a href="{{ route('product.index') }}"><i class="fas fa fa-times-circle"></i></a></h2>                
                    <form action="{{ route('product.update', $selected->id) }}" method="post" enctype="multipart/form-data">
                    @csrf          
                    @method('put')          
                        <div class="form-group">
                            <label for="name">Nama Paket</label>
                            <input type="text" id="name" name="name" class="form-control" value="{{ $selected->name }}">
                            @error('name')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="price">Harga Paket</label>
                            <input type="text" id="price" class="form-control" name="price" value="{{ $selected->price }}">
                            @error('price')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror                           
                        </div>
                        <div class="form-group"> 
                            <label for="quantity">Jumlah Paket</label>
                            <input type="number" id="quantity" class="form-control" name="quantity" value="{{ $selected->quantity }}">
                            @error('quantity')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror         
                        </div>
                        <div class="form-group">
                            <label for="description">Deskripsi</label>
                            <textarea id="description" class="form-control" name="description">{{ $selected->description }}</textarea>
                            @error('description')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="power">Daya</label>
                            <input type="number" id="power" class="form-control" name="power" value="{{ $selected->power }}"> 
                            @error('power')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="type">Jenis</label>
                            <input type="text" id="type" class="form-control" name="type" value="{{ $selected->type }}"> 
                            @error('type')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-block">Submit</button>        
                        </div>
                    </form>   
                    @endif
                </div>
            </div>
            <div class="card mt-3">
                <div class="card-body">
                  
                </div>
            </div>
        </div>
    </div>
</div>

@endsection