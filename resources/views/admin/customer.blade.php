@extends('layouts.admin_app')    
@section('content')

<h1 class="ml-5">Customer</h1>


<!-- Area Chart -->
<div class="col-xl-10 col-lg-7 ml-5">
    <div class="card shadow mb-4">
        <!-- Card Header - Dropdown -->
        <div
            class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <!-- <h6 class="m-0 font-weight-bold text-primary"> admin</h6> -->
            <div class="dropdown no-arrow">
                
            </div>
        </div>
        <!-- Card Body -->
        
        <div class="card-body">
            <div class="row mt-4 ml-md-3 mr-md-3">
                @if(session('info'))
                    <div class="alert alert-success">{{ session('info') }}</div>
                @endif
                <div class="table-responsive">
                    <table class="table table-bordered text-center" id="dataTable" width="100%" cellspacing="0">
                   
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>Tanggal Pinjam</th>
                            <th>Harga</th>
                            <th>Status</th>
                            <th>Nama Packet</th>
                            <th width="20%">Bukti Transaksi</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>      
                    <tbody>
                        @foreach($transactions as $index=>$transaction)
                        <tr>
                            <td>{{ $index + 1 }}</td>
                            <td>{{ $transaction->booking->user_name }}</td>
                            <td>
                                @if($transaction->transaction_date == null)
                                    {{ 'Transaksi Belum Dilakukan' }}
                                @endif
                                {{ $transaction->transaction_date }}
                            </td>
                            <td>Rp. {{ number_format($transaction->payment_amount, 2) }}</td>
                            <td>
                                @if($transaction->transaction_status == '1')
                                    {{'Diproses' }}
                                @endif
                                @if($transaction->transaction_status == '2')
                                    {{'Pending' }}
                                @endif
                                @if($transaction->transaction_status == '3')
                                    {{'Menunggu'}}
                                @endif
                                @if($transaction->transaction_status == '4')
                                    {{'Ditolak'}}
                                @endif
                            </td>
                            <td>{{ $transaction->booking->product_name }}</td>
                            <td>
                                @if($transaction->image == null)
                                    {{'Transaksi Belum Dilakukan'}}
                                @endif
                                @if($transaction->image != null)
                                    <img src="{{ asset('storage/images/' . $transaction->image) }}" alt="Transaction Image" class="img-fluid" style="width: 100%;height:170px;object-fit:contain;">
                                @endif
                            </td>
                            <td>
                                <div class="col-sm justify-content-center">
                                    @if($transaction->transaction_status == '3')
                                        <a href="{{ route('customer.accept', $transaction->id) }}" class="btn btn-primary btn-sm btn-block mb-2">Approve</a>
                                        <a href="{{ route('customer.reject', $transaction->id) }}" class="btn btn-warning btn-sm btn-block mb-2">Tolak</a>                                    
                                    @endif
                                    @if($transaction->transaction_status == '1')
                                        <a href="{{ route('customer.delete', $transaction->id) }}" class="btn btn-danger btn-sm btn-block mb-2">Hapus</a>
                                    @endif
                                    @if($transaction->transaction_status == '4')
                                        <a href="{{ route('customer.delete', $transaction->id) }}" class="btn btn-danger btn-sm btn-block mb-2">Hapus</a>
                                    @endif
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </table>
                    
                </div>
            </div>
        </div>
    </div>
</div>

@endsection


