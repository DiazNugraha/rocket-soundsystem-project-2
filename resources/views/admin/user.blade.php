@extends('layouts.admin_app')
@section('content')
<div>
    <div class="row">
        <div class="col-md-8">
           
            <div class="card shadow mb-4 ml-3">
                <div class="card-body">
                    <h2 class="font-weight-bold mb-3">Pengguna</h2>
                    @if(session('info'))
                        <div class="alert alert-success">{{ session('info') }}</div>
                    @endif
                    <table class="table table-bordered table-hovered table-striped text-center">    
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Email</th>
                                <th>No Telp</th>
                                <th>Aksi</th>                                
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($users as $index=>$user)
                            <tr>
                                <td>{{ $index }}</td>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
                                <td>{{ $user->phone }}</td>
                                <td>
                                    <a href="{{ route('user.delete', $user->id) }}" class="btn btn-danger">Hapus</a>
                                </td>                                
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card shadow mb-4">
                <div class="card-body">
                    <h2 class="font-weight-bold mb-3">Tambah Pengguna</h2>
                    <form action="{{ route('user.create') }}" method="post" enctype="multipart/form-data">
                    @csrf
                        <div class="form-group">
                            <label for="name">Nama</label>
                            <input type="text" id="name" name="name" class="form-control">
                            @error('name')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="text" id="email" class="form-control" name="email">
                            @error('email')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror                           
                        </div>
                        <div class="form-group"> 
                            <label for="phone">Phone</label>
                            <input type="text" id="phone" class="form-control" name="phone">
                            @error('phone')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror         
                        </div>                
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" id="password" class="form-control" name="password">
                            @error('password')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>       
                        <div class="form-group">
                            <label for="confirmPass">Konfirmasi Password</label>
                            <input type="password" id="confirmPass" class="form-control" name="password_confirmation">
                            @error('confirmPass')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-block">Submit</button>        
                        </div>
                    </form>                    
                </div>
            </div>
            <div class="card mt-3">
                <div class="card-body">
                  
                </div>
            </div>
        </div>
    </div>
</div>

@endsection